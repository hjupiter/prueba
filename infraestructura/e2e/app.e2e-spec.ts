import { InfraestructuraPage } from './app.po';

describe('infraestructura App', () => {
  let page: InfraestructuraPage;

  beforeEach(() => {
    page = new InfraestructuraPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
